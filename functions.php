<?php
/**
 * Functions and hooks
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

/**
 * Enqueue parent style sheet
 * as well as the child style sheet
 */
function tcu_commons_child_theme_enqueue_styles() {
	$child_style = 'child-style';

	wp_enqueue_style(
		$child_style,
		get_stylesheet_directory_uri() . '/library/css/style.min.css',
		array( 'tcu-stylesheet' ), '2.3.3'
	);

	// Make sure we only load our JS in the flexible content template.
	if ( is_page_template( 'page-flexible-content.php' ) ) {
		// Adding scripts file in the footer - this file includes ALL our JS files.
		wp_register_script( 'tcu-commons-scripts', get_stylesheet_directory_uri() . '/library/js/min/tcu-commons-scripts.min.js', array( 'jquery', 'jquery-masonry' ), '2.3.3', true );

		wp_enqueue_script( 'tcu-commons-scripts' );
	}

	// Make sure we only load our JS in the showcase page template.
	if ( is_page_template( 'page-showcase.php' ) ) {
		wp_register_script( 'tcu-showcase-scripts', get_stylesheet_directory_uri() . '/library/js/min/all.jquery.showcase.min.js', array( 'jquery' ), '2.3.3', true );

		wp_enqueue_script( 'tcu-showcase-scripts' );
	}

}

add_action( 'wp_enqueue_scripts', 'tcu_commons_child_theme_enqueue_styles' );

/**
 * INCLUDES
 */

require_once 'library/inc/acf-flexible-content-fields.php';
require_once 'library/inc/acf-showcase-page-fields.php';
require_once 'library/inc/page-hero-fields.php';
require_once 'library/inc/tabs-acf-fields.php';
require_once 'library/inc/tcu-breadcrumbs.php';
require_once 'library/inc/acf-theme-settings-fields.php';
require_once 'library/inc/tcu-theme-settings.php';

/***************************** ACF Plugin *****************************/

/**
 * Check if the ACF plugin is intalled
 *
 * @return bool  True if the ACF plugin is installed.
 */
function tcu_commons_child_theme_acf() {
	// Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function.
	if ( ! function_exists( 'is_plugin_active' ) ) {
		include_once ABSPATH . 'wp-admin/includes/plugin.php';
	}

	// Checks to see if the acf pro plugin is activated.
	if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) || is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Admin notice to display when ACF is not installed
 */
function tcu_commons_child_theme_notice_missing_acf() {
	global $tcu_commons_child_theme_notice_msg;

	$allowed_tags = array(
		'div' => array(
			'class' => array(),
		),
	);

	$tcu_commons_child_theme_notice_msg = esc_html__( 'The TCU Commons Child Theme needs "Advanced Custom Fields PRO" plugin to run. Please download and activate it.', 'tcu_commons_child_theme' );

	if ( ! tcu_commons_child_theme_acf() ) {
		echo wp_kses( '<div class="notice notice-error notice-large"><div class="notice-title">' . $tcu_commons_child_theme_notice_msg . '</div></div>', $allowed_tags );
	}
}

add_action( 'admin_notices', 'tcu_commons_child_theme_notice_missing_acf' );

// Thumbnail sizes.
add_image_size( 'tcu-550-200', 550, 200, true );
add_image_size( 'tcu-480-550', 480, 550, true );
add_image_size( 'tcu-700-550', 700, 550, true );
add_image_size( 'tcu-1000-550', 1000, 550, true );
add_image_size( 'tcu-1800-550', 1800, 550, true );
add_image_size( 'tcu-320-320', 320, 320, true );

/**
 * Hide ACF except for Super Admins and Admins
 *
 * @param string $show Capabilities string.
 */
function tcu_acf_show_admin( $show ) {
	$show = current_user_can( 'manage_options' );
	return $show;
}

add_filter( 'acf/settings/show_admin', 'tcu_acf_show_admin' );

/**
 * TCU News Custom Post Type
 * Select keywords in our flexible content page template
 *
 * @param  array $field  The field settings array.
 * @return array  $field  Modified settings array.
 */
function tcu_acf_load_news_keyword_choices( $field ) {

	// Bail if TCU News is not installed.
	if ( ! function_exists( 'tcu_news_query' ) ) {
		return;
	}

	// Reset choices.
	$field['choices'] = array();

	// Get all keywords.
	$keywords = get_terms(
		array(
			'taxonomy'   => 'tcu_news_keyword',
			'hide_empty' => false,
		)
	);

	// Make sure we have the default selection.
	$new_choices = array( 'View all news ( tcu_all_news )' );

	foreach ( $keywords as $key ) {
		array_push( $new_choices, $key->name . ' (' . $key->slug . ')' );
	}

	if ( is_array( $new_choices ) ) {
		foreach ( $new_choices as $key ) {
			$field['choices'][ $key ] = $key;
		}
	} else {
		$field['choices'][ $new_choices ] = $new_choices;
	}

	// Return the field.
	return $field;
}

add_filter( 'acf/load_field/name=the_commons_tcu_news_section_select_keyword', 'tcu_acf_load_news_keyword_choices' );

/********************* TCU RSS NEWS  ******************************/

/**
 * Process RSS feed and
 * create a transient to cache results
 *
 * @param string $rss URL for the RSS feed.
 * @param int    $num The number of stories to fetch.
 */
function tcu_process_rss_feed( $rss, $num = 5 ) {

	// Allowed HTML tags.
	$allowed_tags = array(
		'div' => array(
			'class' => array(),
			'id'    => array(),
			'style' => array(),
		),
		'p'   => array(
			'class' => array(),
		),
		'img' => array(
			'class'  => array(),
			'src'    => array(),
			'height' => array(),
			'width'  => array(),
			'alt'    => array(),
		),
		'h5'  => array(
			'class' => array(),
		),
		'a'   => array(
			'aria-label' => array(),
			'title'      => array(),
			'class'      => array(),
			'href'       => array(),
		),
		'svg' => array(
			'xmls'   => array(),
			'height' => array(),
			'width'  => array(),
			'style'  => array(),
		),
		'use' => array(
			'xlink:href' => array(),
		),
	);

	// Get a SimplePie feed object from the specified feed source.
	$rss = fetch_feed( $rss );

	$maxitems = 0;

	// Checks that the object is created correctly.
	if ( ! is_wp_error( $rss ) ) {

		// Figure out how many total items there are, but limit it.
		$maxitems = $rss->get_item_quantity( $num );

		// Build an array of all the items, starting with element 0.
		$rss_items = $rss->get_items( 0, $maxitems );
	}

	$story = '<div class="tcu-masonry-home effect-2 cf" id="tcu-masonry-home">';

		// If RSS feed is empty.
	if ( 0 === $maxitems ) {
		$story .= '<p>' . esc_html_e( 'No news items at the moment.', 'tcu_commons_child_theme' ) . '</p>';
	} else {

		// Loop through each feed item.
		foreach ( $rss_items as $item ) {

			// Grab the entire content.
			$content = $item->get_content();
			$img     = false;

			// Let's grab the IMG tag with class name attachment-tcu_news_thumb from inside the content.
			$img = ( preg_match( '/(<img[^>]+>)/i', $content, $matches ) ) ? $matches[0] : false;

			$story .= '<div class="tcu-article tcu-modal cf">';
			if ( $img ) {
				$story .= $img;
			}
			$story .= '<div style="clear: both;" class="tcu-modal__content cf">';
			$story .= '<h5 class="tcu-arvo tcu-mar-b0 h4">' . ent2ncr( $item->get_title() ) . '</h5>';
			$story .= '<p class="tcu-byline">' . ent2ncr( $item->get_date( 'j F Y' ) ) . '</p>';

			$story .= substr( ent2ncr( strip_tags( $item->get_content() ) ), 0, 150 ) . '...';
			$story .= '</div>';
			$story .= '<a aria-label="' . esc_attr__( 'Read more about ', 'tcu_commons_child_theme' ) . esc_attr( ent2ncr( $item->get_title() ) ) . '" title="' . esc_attr( $item->get_title() ) . '" class="tcu-button tcu-button--primary tcu-full-width" href="' . esc_url( $item->get_permalink() ) . '">Read More<svg height"30" width="30"><use xlink:href="#play-icon"></use></svg></a>';
			$story .= '</div><!-- end of .tcu-article -->';
		}
	}

	$story .= '</div><!-- end of .tcu-masonry -->';

	echo wp_kses( $story, $allowed_tags );

} // don't remove this bracket!

/**
 * Return the HTML for the Hero Image section
 *
 * @param string $title     Title of section.
 * @param string $image     The URL of the image.
 * @param string $content   The content.
 * @param string $link      The URL for hyperlink.
 * @param string $text      The text for the URL.
 * @param string $arialabel The aria label.
 */
function tcu_hero_image_template( $title, $image, $content, $link, $text, $arialabel ) {

	/**
	* We inlined our styles in order to change the image size for each Media Query
	* Faster loading time and smaller images for mobile
	* Performance is important!
	*/
	?>
	<style type="text/css">
		/* <![CDATA[ */
		.tcu-hero {
			background: url('<?php echo esc_url( $image['sizes']['tcu-480-550'] ); ?>') center center no-repeat;
			background-size: cover;
		}

		@media screen and ( min-width: 481px ) {
			.tcu-hero {
				background: url('<?php echo esc_url( $image['sizes']['tcu-1000-550'] ); ?>') center center no-repeat;
				background-size: cover;
			}
		}

		@media screen and ( min-width: 1000px ) {
		.tcu-hero {
				background: url('<?php echo esc_url( $image['sizes']['tcu-1800-550'] ); ?>') center center no-repeat;
				background-size: cover;
			}
		}

		@media screen and ( min-width: 1200px ) {
		.tcu-hero {
				background: url('<?php echo esc_url( $image['url'] ); ?>') center center no-repeat;
				background-size: cover;
			}
		}
		/* ]]> */
	</style>

	<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">

		<div class="tcu-hero tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align">

			<div class="tcu-hero__content tcu-layout-center">

			<?php if ( $title ) : ?>
				<h1 class="tcu-arvo tcu-border--purple tcu-font-bold tcu-text-shadow"><?php echo esc_html( $title ); ?></h1>
			<?php
			endif;

			// @codingStandardsIgnoreLine
			if ( $content ) :
			?>
				<div class="h4 tcu-cabin cf"><?php echo wp_kses_post( $content ); ?></div>
			<?php
			endif;

			// @codingStandardsIgnoreLine
			if ( $link && $arialabel ) :
			?>
				<a aria-label="<?php echo esc_attr( $arialabel ); ?>" class="tcu-cabin tcu-font-bold tcu-top16" href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $text ); ?></a>
			<?php elseif ( $link ) : ?>
				<a class="tcu-cabin tcu-font-bold tcu-top16" href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $text ); ?></a>
			<?php endif; ?>

			</div><!-- end of tcu-hero -->

		</div><!-- end of .tcu-hero -->

	</div><!-- end of tcu-layoutwrap--purple -->

<?php
} // don't remove this bracket!

/**
 * Return the HTML for the Showcase section
 *
 * @param array  $rows     The ACF rows.
 * @param string $heading The title.
 * @param array  $image   The rows image index  name.
 * @param string $title   The rows title index name.
 * @param string $content The row textarea index name.
 * @param string $link    The row URL index name.
 * @param string $text    The row URL index name.
 * @param string $arialabel The rows arialabel.
 */
function tcu_showcase_template( $rows, $heading, $image, $title, $content, $link, $text, $arialabel ) {
?>
	<div class="tcu-background--shattered">

		<div class="tcu-layout--large tcu-layout-center cf">

			<h1 class="tcu-mar-t0 tcu-mar-t0 tcu-arvo tcu-alignc tcu-font-bold tcu-border--purple h2"><?php echo esc_html( $heading ); ?></h1>

			<div style="max-width: 1330px;" class="tcu-layout-center tcu-background--grunge tcu-showcase-wrapper size1of1 m-size1of1 cf">

			<?php
			// Let's grab our array of content.
			$the_rows = $rows;

			if ( $the_rows ) :

				foreach ( $the_rows as $row ) :

					$the_image     = $row[ $image ];
					$the_title     = $row[ $title ];
					$the_content   = $row[ $content ];
					$the_link      = $row[ $link ];
					$the_linktext  = $row[ $text ];
					$the_arialabel = $row[ $arialabel ];
					?>

				<div class="tcu-showcase unit size1of2 m-size1of1 cf">

					<input alt="<?php echo esc_attr( $the_title ); ?>" tabindex="-1" type="image" class="tcu-showcase__image" src="<?php echo esc_url( $the_image['sizes']['tcu-320-320'] ); ?>">
					</input>

					<div class="tcu-showcase__content">

						<?php if ( $the_title ) : ?>
							<h4 class="tcu-mar-t0"><?php echo esc_html( $the_title ); ?></h4>
						<?php endif; ?>

						<p><?php echo esc_textarea( $the_content ); ?></p>

						<?php if ( $the_link && $the_arialabel ) : ?>

							<a title="<?php echo esc_attr( $the_title ); ?>" aria-label="<?php echo esc_attr( $the_arialabel ); ?>" href="<?php echo esc_url( $the_link ); ?>" class="tcu-button tcu-button--transparent tcu-alignc tcu-full-width"><?php echo esc_html( $the_linktext ); ?></a>

						<?php elseif ( $the_link ) : ?>

							<a title="<?php echo esc_attr( $the_title ); ?>" href="<?php echo esc_url( $the_link ); ?>" class="tcu-button tcu-button--transparent tcu-alignc tcu-full-width"><?php echo esc_html( $the_linktext ); ?></a>

						<?php endif; ?>

					</div><!-- end of .tcu-showcase__content -->

				</div><!-- end of .tcu-showcase -->

				<?php
				// End of the ACF loop.
				endforeach;
				?>

			</div><!-- end of .tcu-background--grunge -->

		</div><!-- end of .tcu-layout--large -->

	</div><!-- end of .tcu-background--shattered -->
	<?php endif ?>

<?php
} // don't remove this bracket!

/**
 * Let's grab the values for the theme settings page.
 */
$tcu_commons_search_form_replace = get_field( 'the_commons_search_form', 'options' );

if ( $tcu_commons_search_form_replace ) {

	/**
	 * Let's override the parent search form
	 *
	 * @param string $form HTML for the form.
	 */
	function tcu_wpsearch( $form ) {

		// Setting page values.
		$tcu_commons_search_frontend_name   = get_field( 'tcu_commons_frontend_name', 'options' );
		$tcu_commons_search_collection_name = get_field( 'the_commons_collection_name', 'options' );

		$form = '<form role="search" class="tcu-searchform cf" action="https://search.tcu.edu/search?" method="get" name="search">
					<label class="screen-reader-text tcu-visuallyhidden tcu-searchform__label" for="q">' . __( 'Search for:', 'tcu_commons_child_theme' ) . '</label>
					<input class="tcu-searchform__text" type="search" value="' . get_search_query() . '" name="q" id="q"  placeholder="' . esc_attr__( 'Search the Site...', 'tcu_commons_child_theme' ) . '" />
					<input title="Search" class="tcu-searchform__submit" type="submit" value="Go">
					<input type="hidden" value="date:D:L:d1" name="sort" />
					<input type="hidden" value="xml_no_dtd" name="output" />
					<input type="hidden" value="UTF-8" name="oe" />
					<input type="hidden" value="UTF-8" name="ie" />
					<input type="hidden" value="default_frontend" name="client" />
					<input type="hidden" value="' . esc_attr( $tcu_commons_search_frontend_name ) . '" name="proxystylesheet" />
					<input type="hidden" value="' . esc_attr( $tcu_commons_search_collection_name ) . '" name="site" />
				</form>';

		return $form;
	}
}
