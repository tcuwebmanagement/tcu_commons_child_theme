<?php
/**
 * Template Name: Right Sidebar (Showcase)
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

get_header();

if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<?php get_template_part( 'partials/content', 'pageshowcase' ); ?>

<div class="tcu-layoutwrap--transparent">

		<div class="tcu-layout-constrain cf">

			<main class="unit size2of3 m-size2of3 cf" id="main">

				<?php
				if ( have_posts() ) :

					/**
					 * Start the WP loop.
					 */
					while ( have_posts() ) :
						the_post();
				?>

				<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article cf' ); ?>>

					<div class="tcu-article__content cf">

						<h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>

						<?php
						/**
						 * Display the content.
						 */
						the_content();
						?>

					</div><!-- end of .tcu-article__content -->

				</article><!-- end of .tcu-entry-content -->

				<?php

				/**
				 * End of the WP loop
				 */
				endwhile;

						else :

							// If no content, include the "No posts found" template.
							get_template_part( 'partials/content', 'none' );

				endif;
				?>

			</main><!-- end of .unit -->

			<?php get_sidebar(); ?>

		</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
