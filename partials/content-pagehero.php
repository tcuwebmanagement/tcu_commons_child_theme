<?php
/**
 * Template part to display the hero image section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// ACF Variables.
$tcu_title     = get_field( 'the_commons_page_hero_heading' );
$tcu_image     = get_field( 'the_commons_page_hero_image' );
$tcu_content   = get_field( 'the_commons_page_hero_image_content' );
$tcu_link      = get_field( 'the_commons_page_hero_link' );
$tcu_text      = get_field( 'the_commons_page_hero_link_text' );
$tcu_arialabel = get_field( 'the_commons_page_hero_aria_label' );

/*
* Returns the HTML component of the hero image
* located in functions.php
*/
tcu_hero_image_template( $tcu_title, $tcu_image, $tcu_content, $tcu_link, $tcu_text, $tcu_arialabel );

