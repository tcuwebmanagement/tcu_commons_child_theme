<?php
/**
 * Template part to display full width visual editor
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

$tcu_title   = get_sub_field( 'tcu_commons_visual_editor_heading' );
$tcu_content = get_sub_field( 'the_commons_visual_editor_content' );
?>
<div class="tcu-layoutwrap--transparent">

	<div class="tcu-dash-background-svg">

		<div class="tcu-layout-constrain inner-content cf">

			<div class="unit size1of1 m-size1of1 tcu-below16 cf">

				<div class="tcu-article__content cf">

					<h3 class="tcu-mar-t0 tcu-arvo tcu-font-bold tcu-border--purple tcu-alignc h2"><?php echo esc_html( $tcu_title ); ?></h3>

					<?php echo do_shortcode( $tcu_content ); ?>

				</div>

			</div><!-- end of .unit -->

		</div><!-- end of .tcu-layout-constrain -->

	</div><!-- end of .tcu-dash-background-svg -->

</div><!-- end .tcu-layoutwrap--transparent -->
