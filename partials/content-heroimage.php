<?php
/**
 * Template part to display the hero image section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// ACF Variables.
$tcu_title     = get_sub_field( 'the_commons_large_hero_image_section_title' );
$tcu_image     = get_sub_field( 'the_commons_large_hero_image_section_image' );
$tcu_content   = get_sub_field( 'the_commons_large_hero_image_section_content' );
$tcu_link      = get_sub_field( 'the_commons_large_hero_image_section_link' );
$tcu_text      = get_sub_field( 'the_commons_large_hero_image_section_link_text' );
$tcu_arialabel = get_sub_field( 'the_commons_large_hero_aria_label' );

/*
* Returns the HTML component of the hero image
* located in functions.php
*/
tcu_hero_image_template( $tcu_title, $tcu_image, $tcu_content, $tcu_link, $tcu_text, $tcu_arialabel );
