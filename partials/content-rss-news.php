<?php
/**
 * Template part to display RSS News feed
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

$tcu_title     = get_sub_field( 'rss_tcu_news_feed_title' );
$tcu_rss       = get_sub_field( 'rss_tcu_news_feed_url' );
$tcu_maxnum    = get_sub_field( 'rss_tcu_news_feed_items' );
$tcu_more_link = get_sub_field( 'rss_tcu_news_feed_read_more' );
?>

<div class="tcu-layoutwrap--transparent cf">

	<div class="tcu-layout-constrain cf">

		<?php if ( $tcu_title ) : ?>
			<h3 class="tcu-mar-t0 tcu-mar-t0 tcu-arvo tcu-font-bold tcu-alignc h2 tcu-border--purple"> <?php echo esc_html( $tcu_title ); ?></h3>
		<?php endif; ?>

		<?php tcu_process_rss_feed( $tcu_rss, $tcu_maxnum ); ?>

		<div class="tcu-layout-center tcu-alignc tcu-top32 tcu-below32 cf">
			<!-- Our Read More button -->
			<a title="Read More News" class="tcu-button tcu-button--primary tcu-alignc" href="<?php echo esc_url( $tcu_more_link ); ?>">View More</a>
		</div>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->
