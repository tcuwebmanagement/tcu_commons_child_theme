<?php
/**
 * Template part to display Highlight box section (Three Columns)
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

if ( have_rows( 'the_commons_highlight_sections_three_columns_hightlight_section_repeat' ) ) : ?>

<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-pad-lr0 cf">

	<div class="tcu-layout--large tcu-layout-center">

	<?php
	/**
	 * Start the ACF loop.
	 */
	while ( have_rows( 'the_commons_highlight_sections_three_columns_hightlight_section_repeat' ) ) :
		the_row();

		// ACF Variables.
		$tcu_image     = get_sub_field( 'the_commons_highlight_sections_three_columns_image' );
		$tcu_title     = get_sub_field( 'the_commons_highlight_sections_three_columns_title' );
		$tcu_link      = get_sub_field( 'the_commons_highlight_sections_three_columns_link' );
		$tcu_text      = get_sub_field( 'the_commons_highlight_sections_three_column_link_text' );
		$tcu_arialabel = get_sub_field( 'the_commons_highlight_sections_three_columns_aria_label' );
?>

<?php
/**
 * We inlined our styles in order to change the image size for each media query
 * Faster loading time and smaller images for mobile
 * Performance is important!
 */
if ( ! empty( $tcu_image ) ) :
?>
<style type="text/css">
	/* <![CDATA[ */
	#tcu-highlight-<?php echo esc_html( $tcu_image['name'] ); ?> {
		background: #111111 url('<?php echo esc_html( $tcu_image['sizes']['tcu-480-550'] ); ?>') center center no-repeat;
	}
	/* ]]> */
</style>
<?php endif; ?>

	<div id="tcu-highlight-<?php echo esc_attr( $tcu_image['name'] ); ?>" class="unit size1of3 m-size1of1 tcu-background-defaults">

		<div class="tcu-zindex-2 tcu-absolute-btm-left">

			<?php if ( $tcu_title ) : ?>
				<h2 class="tcu-font-bold"><?php echo esc_html( $tcu_title ); ?></h2>
			<?php endif; ?>

			<?php if ( $tcu_arialabel ) : ?>

				<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--transparent" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>

			<?php else : ?>

				<a title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--transparent" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>

			<?php endif; ?>

		</div><!-- end of .tcu-zindex-2 -->

	</div><!-- end of .tcu-background-defaults -->

	<?php
	/**
	 * End of the ACF loop.
	 */
	endwhile;
	?>

	</div><!-- end ot .tcu-layout-center -->

</div><!-- end of .tcu-layoutwrap--grey -->

<?php endif; ?>
