<?php
/**
 * Template part to display Highlight box section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

if ( have_rows( 'the_commons_highlight_sections_two_columns_hightlight_section_repeat' ) ) : ?>

<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-pad-lr0 cf">

	<div class="tcu-layout--large tcu-layout-center cf">

	<?php
	/**
	 * Start the ACF loop.
	 */
	while ( have_rows( 'the_commons_highlight_sections_two_columns_hightlight_section_repeat' ) ) :
		the_row();

		// ACF Variables.
		$tcu_image     = get_sub_field( 'the_commons_highlight_sections_two_columns_image' );
		$tcu_medium    = $tcu_image['sizes']['tcu-700-550'];
		$tcu_post_id   = $tcu_image['name'];
		$tcu_title     = get_sub_field( 'the_commons_highlight_sections_two_columns_title' );
		$tcu_content   = get_sub_field( 'the_commons_highlight_sections_two_columns_content' );
		$tcu_link      = get_sub_field( 'the_commons_highlight_sections_two_columns_link' );
		$tcu_text      = get_sub_field( 'the_commons_highlight_sections_two_columns_link_text' );
		$tcu_arialabel = get_sub_field( 'the_commons_highlight_sections_two_columns_aria_label' );

		/**
		 * Inlined styles in order to change the image size for each media query
		 * Faster loading time and smaller images for mobile
		 * Performance is important!
		 */
	?>
	<?php if ( ! empty( $tcu_image ) ) : ?>
	<style>
	/* <![CDATA[ */
	#tcu-highlight-<?php echo esc_attr( $tcu_post_id ); ?> {
		background: linear-gradient(rgba(77, 25, 121, 0.65), rgba(77, 25, 121, 0.65)), #4D1979 url('<?php echo esc_url( $tcu_medium ); ?>') center center no-repeat;
	}

	#tcu-highlight-<?php echo esc_attr( $tcu_post_id ); ?>:nth-child(3n+2) {
		background: linear-gradient(rgba(0, 0, 0, 0.65), rgba(0, 0, 0, 0.65)), #111111 url('<?php echo esc_url( $tcu_medium ); ?>') center center no-repeat;
	}
	/* ]]> */
	</style>
	<?php endif; ?>

	<div id="tcu-highlight-<?php echo esc_attr( $tcu_post_id ); ?>" class="unit size1of2 m-size1of1 tcu-background-defaults">

		<div class="tcu-zindex-2">

			<?php if ( $tcu_title ) : ?>
				<h2 class="tcu-uppercase h2"><?php echo esc_html( $tcu_title ); ?></h2>
			<?php endif; ?>

			<?php if ( $tcu_content ) : ?>
				<p><?php echo esc_textarea( $tcu_content ); ?></p>
			<?php endif; ?>

			<?php if ( $tcu_arialabel ) : ?>

				<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--transparent" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?> <svg focusable="false" height="30" width="30" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>

			<?php else : ?>

				<a title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--transparent" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?> <svg focusable="false" height="30" width="30" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>

			<?php endif; ?>

		</div><!-- end of .tcu-zindex-2 -->

	</div><!-- end of .tcu-background-defaults -->

	<?php
	/**
	 * End of the ACF loop.
	 */
	endwhile;
	?>

	</div>

</div><!-- end of .tcu-layoutwrap--grey -->

<?php endif; ?>
