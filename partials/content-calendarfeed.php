<?php
/**
 * Template part to display a feed for the Content Area
 *
 * IMPORTANT: Originally this partial was meant to display
 * a feed from Active Data Calendar. It was quickly adapted
 * for shortcode use. We did not change the ACF names for
 * backward compatibility
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// ACF Variables.
$tcu_title     = get_sub_field( 'the_commons_active_calendar_feed_title' );
$tcu_content   = get_sub_field( 'the_commons_active_calendar_feed_calendar_feed' );
$tcu_link      = get_sub_field( 'the_commons_active_calendar_feed_link' );
$tcu_text      = get_sub_field( 'the_commons_active_calendar_feed_link_text' );
$tcu_arialabel = get_sub_field( 'the_commons_active_calendar_feed_aria_label' );

?>
<div class="tcu-layoutwrap--transparent tcu-background--feathers">

	<div class="tcu-layout-constrain cf">

		<?php if ( $tcu_title ) : ?>
			<h3 class="tcu-mar-t0 tcu-arvo tcu-font-bold tcu-border--purple tcu-alignc h2">
				<?php echo esc_html( $tcu_title ); ?>
			</h3>
		<?php endif; ?>

		<div style="max-width: 1100px;" class="tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align tcu-article__content">
			<?php echo do_shortcode( $tcu_content ); ?>
		</div>

		<?php if ( $tcu_link && $tcu_arialabel ) : ?>

			<div class="tcu-layout-center size1of3 m-size1of4 tcu-top32 tcu-below32">
				<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--primary tcu-full-width tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
			</div>

		<?php elseif ( $tcu_link ) : ?>

			<div class="tcu-layout-center size1of3 m-size1of4 tcu-top32 tcu-below32">
				<a title="<?php echo esc_attr( $tcu_title ); ?>" class="tcu-button tcu-button--primary tcu-full-width tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
			</div>

		<?php endif; ?>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->
