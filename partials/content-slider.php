<?php
/**
 * Template part to display the slider
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// check if the flexible content field has rows of data & loop.
if ( have_rows( 'the_commons_large_slider_section_repeater' ) ) : ?>

<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">

	<div class="tcu-layout--large tcu-layout-center">

		<div class="tcu-commons-slider">

		<?php
		/**
		 * Start the ACF loop.
		 */
		while ( have_rows( 'the_commons_large_slider_section_repeater' ) ) :
			the_row();

			// ACF Variables.
			$tcu_image     = get_sub_field( 'the_commons_large_slider_section_background_image' );
			$tcu_link      = get_sub_field( 'the_commons_large_slider_section_link' );
			$tcu_text      = get_sub_field( 'the_commons_large_slider_section_link_text' );
			$tcu_arialabel = get_sub_field( 'the_commons_large_slider_section_aria_label' );

		?>

			<div class="tcu-commons-slider-body">

				<div class="tcu-commons-slider-image" style="background-image: url('<?php echo esc_url( $tcu_image['url'] ); ?>'); background-size: cover;"></div>
				<!-- end of .commons-slider-image -->

				<div class="tcu-commons-slider-content tcu-overlay--purple">

					<h1 class="tcu-mar-t0 tcu-arvo tcu-font-bold h2"><?php the_sub_field( 'the_commons_large_slider_section_title' ); ?></h1>

					<?php the_sub_field( 'the_commons_large_slider_section_content' ); ?>

					<?php if ( $tcu_link && $tcu_arialabel ) : ?>

						<div class="tcu-layout-center tcu-alignc tcu-full-width tcu-top32">
							<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" class="tcu-button tcu-button--secondary tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
						</div>

					<?php elseif ( $tcu_link ) : ?>

						<div class="tcu-layout-center tcu-alignc tcu-full-width tcu-top32">
							<a class="tcu-button tcu-button--secondary tcu-alignc" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
						</div>

					<?php endif; ?>

				</div><!-- .tcu-commons-slider-content -->

			</div><!-- end of .tcu-commons-body -->

		<?php
		/**
		 * End the ACF loop.
		 */
		endwhile;
		?>

		</div><!-- end of .tcu-commons-slider -->

	</div><!-- end of .tcu-layout--large -->

</div><!-- end of .tcu-layoutwrap--purple -->

<?php endif; ?>
