<?php
/**
 * Template partial to display Showcase section in flexible content template
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

$tcu_rows      = get_sub_field( 'showcase_block_repeater' );
$tcu_heading   = get_sub_field( 'the_commons_showcase_title' );
$tcu_image     = 'showcase_section_image';
$tcu_title     = 'showcase_section_title';
$tcu_content   = 'showcase_section_content';
$tcu_text      = 'showcase_section_link_text';
$tcu_link      = 'showcase_section_url';
$tcu_arialabel = 'showcase_section_aria_label';

/*
 * Returns the HTML component of the showcase
 * located in functions.php
 */
tcu_showcase_template( $tcu_rows, $tcu_heading, $tcu_image, $tcu_title, $tcu_content, $tcu_link, $tcu_text, $tcu_arialabel );

