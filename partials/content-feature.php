<?php
/**
 * Template part to display feature section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// ACF variables.
$tcu_content   = get_sub_field( 'the_commons_feature_section_content' );
$tcu_link      = get_sub_field( 'the_commons_feature_section_link' );
$tcu_arialabel = get_sub_field( 'the_commons_feature_section_aria_label' );
?>
<div class="tcu-layoutwrap--purple tcu-background--sayagata">

	<div class="tcu-layout--large tcu-flexbox tcu-flexbox--column tcu-flexbox--align-items tcu-flexbox--vertical-align tcu-layout-center">

		<div style="max-width: 850px;" class="tcu-layout-constrain tcu-layout-center tcu-article__content">

			<?php
			if ( $tcu_content ) :
				echo do_shortcode( $tcu_content );
			endif;

			if ( $tcu_link && $tcu_arialabel ) :
			?>

				<p class="tcu-alignc">
					<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" class="tcu-button tcu-button--transparent tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>">Learn More <svg focusable="false" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>
				</p>

			<?php elseif ( $tcu_link ) : ?>

				<p class="tcu-alignc">
					<a class="tcu-button tcu-button--transparent tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>">Learn More <svg focusable="false" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>
				</p>

			<?php endif; ?>

		</div><!-- end of .tcu-layout-constrain -->

	</div><!-- end of .tcu-layout--large -->

</div><!-- end of .tcu-layoutwrap-purple -->
