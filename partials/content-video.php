<?php
/**
 * Template part to display video section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// Variables.
$tcu_video     = get_sub_field( 'the_commons_large_video_section_video' );
$tcu_content   = get_sub_field( 'the_commons_large_video_section_content' );
$tcu_text      = get_sub_field( 'the_commons_large_video_section_link_text' );
$tcu_link      = get_sub_field( 'the_commons_large_video_section_link' );
$tcu_arialabel = get_sub_field( 'the_commons_large_video_aria_label' );
$tcu_src       = ( preg_match( '/src="(.+?)"/', $tcu_video, $matches ) ) ? $matches[1] : '';
$tcu_allowed   = array(
	'iframe' => array(
		'id'              => array(),
		'src'             => array(),
		'allow'           => array(),
		'encrypted-media' => array(),
		'allowfullscreen' => array(),
		'tabindex'        => array(),
		'style'           => array(),
		'frameborder'     => array(),
		'height'          => array(),
		'width'           => array(),
	),
);

// Find the video ID.
preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $tcu_src, $match );

// add extra params to iframe src.
$tcu_params = array(
	'controls'    => 0,
	'showinfo'    => 0,
	'autoplay'    => 1,
	'loop'        => 1,
	'rel'         => 0,
	'hd'          => 1,
	'enablejsapi' => 1,
	'autohide'    => 1,
	'playlist'    => $match[1],
);

// rebuild our URL.
$tcu_new_src = add_query_arg( $tcu_params, $tcu_src );

// add extra params to iframe src.
$tcu_video = str_replace( $tcu_src, $tcu_new_src, $tcu_video );

// add extra attributes to iframe html.
$tcu_attributes = 'frameborder="0" tabindex="-1" id="ytplayer"';

$tcu_video = str_replace( '></iframe>', ' ' . $tcu_attributes . '></iframe>', $tcu_video );

// Mute our video with the script below.
?>
<script type="text/javascript">
	//<![CDATA[
	var tag = document.createElement('script');
	tag.src = "//www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	function onYouTubePlayerAPIReady() {

		player = new YT.Player('ytplayer', {
			events: {
				'onReady': function(e) {
					e.target.mute();
				}
			}
		});
	}
	//]]>
</script>

<!-- Large Video  -->
<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">

	<div class="tcu-video cf">

		<div class="tcu-video-container">

			<?php echo wp_kses( $tcu_video, $tcu_allowed ); ?>

		</div><!-- end of tcu-video-container -->

		<div class="tcu-hero__content tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align">

			<?php if ( $tcu_content ) : ?>

				<h1 class="tcu-arvo tcu-font-bold tcu-text-shadow h1"><?php echo esc_html( $tcu_content ); ?></h1>

			<?php endif; ?>

			<?php if ( $tcu_link && $tcu_arialabel ) : ?>

				<div class="tcu-layout-center">
					<a aria-label="<?php echo esc_attr( $tcu_arialabel ); ?>" class="tcu-button tcu-button--primary tcu-bounce tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
				</div>

			<?php elseif ( $tcu_link ) : ?>

				<div class="tcu-layout-center">
					<a class="tcu-button tcu-button--primary tcu-bounce tcu-top16" href="<?php echo esc_url( $tcu_link ); ?>"><?php echo esc_html( $tcu_text ); ?></a>
				</div>

			<?php endif; ?>

		</div><!-- end of tcu-hero__content -->

	</div><!-- end of .tcu-video -->

</div><!-- end of tcu-layoutwrap--purple -->
