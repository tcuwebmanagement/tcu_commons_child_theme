<?php
/**
 * Template part to display tab section
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

// ACF Variables.
$tcu_title   = get_sub_field( 'the_commons_tabs_section_title' );
$tcu_content = get_sub_field( 'the_commons_tabs_section_content' );
?>

<div class="tcu-layoutwrap--transparent tcu-below32 cf">

	<div class="tcu-layout-constrain tcu-layout-center cf">

		<div class="unit size1of1 m-size1of1">

			<div class="tcu-article__content tcu-below32">

				<?php if ( $tcu_title ) : ?>
					<h3 class="tcu-arvo tcu-font-bold h2"><?php echo esc_html( $tcu_title ); ?></h3>
				<?php
				endif;

				/**
				 * Display content
				 */
				if ( $tcu_content ) :
					echo wp_kses_post( $tcu_content );
				endif;

				?>
			</div><!-- end of .tcu-article__content -->

			<?php
			// check if the flexible content field has rows of data & loop through rows.
			if ( have_rows( 'the_commons_tabs_section_tabs_repeater' ) ) :
			?>

			<!-- Start our Tabs -->
			<div class="tcu-responsive-tabs">

				<!-- Begin of ul - responsive tabs -->
				<ul>

				<?php
				/**
				 * Start the ACF loop.
				 */
				while ( have_rows( 'the_commons_tabs_section_tabs_repeater' ) ) :
					the_row();

					// Variables.
					$tcu_tabs_object = get_sub_field_object( 'the_commons_tabs_section_tabs_repeater_title' );
					$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
					$tcu_tabs_title = preg_replace( '![^a-z0-9]+!i', '_', get_sub_field( 'the_commons_tabs_section_tabs_repeater_title' ) )  . '_' . $tcu_tabs_hash;
				?>

					<li><a href="#<?php echo esc_attr( strtolower( $tcu_tabs_title ) ); ?>"><?php the_sub_field( 'the_commons_tabs_section_tabs_repeater_title' ); ?></a></li>

				<?php
				/**
				 * End the ACF loop.
				 */
				endwhile;
				?>

				</ul><!-- End of ul - responsive tabs -->

				<?php
				/**
				 * Start the ACF loop.
				 */
				while ( have_rows( 'the_commons_tabs_section_tabs_repeater' ) ) :
					the_row();

					// Variables.
					$tcu_tabs_object = get_sub_field_object( 'the_commons_tabs_section_tabs_repeater_title' );
					$tcu_tabs_hash = hash('crc32b', $tcu_tabs_object['name']);
					$tcu_tabs_title = preg_replace( '![^a-z0-9]+!i', '_', get_sub_field( 'the_commons_tabs_section_tabs_repeater_title' ) )  . '_' . $tcu_tabs_hash;
				?>

				<!-- Start Content Section -->
				<div id="<?php echo esc_attr( strtolower( $tcu_tabs_title ) ); ?>" class="cf"><?php the_sub_field( 'the_commons_tabs_section_tabs_repeater_content' ); ?></div>

				<?php
				/**
				 * End the ACF loop.
				 */
				endwhile;
				?>

			</div><!-- end of .responsive-tabs -->

			<?php endif; ?>

		</div><!-- end of .size1of2 -->

	</div><!-- end of .tcu-layout--large -->

</div><!-- end of .tcu-layoutwrap--grey -->
