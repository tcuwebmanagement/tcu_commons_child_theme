<?php
/**
 * The template for displaying a message that posts cannot be found
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

?>
<div class="tcu-layoutwrap--transparent tcu-pad-lr0 cf">

	<div class="tcu-layout-constrain tcu-infographics cf">

		<div class="tcu-article hentry cf">

		<header class="tcu-article__header">
			<h1 class="h1"><?php esc_html_e( 'Oops, Post Not Found!', 'tcu_commons_child_theme' ); ?></h1>
		</header>

		<div class="tcu-article__content">
			<p><?php esc_html_e( 'Uh Oh. Something is missing. Try double checking things.', 'tcu_commons_child_theme' ); ?></p>
		</div>

		</div>

	</div><!-- end of .tcu-layout-contrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->
