<?php
/**
 * Partial to display showcase in Default Template (Showcase) page
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

$tcu_rows      = get_field( 'showcase_page_repeater' );
$tcu_heading   = get_field( 'showcase_page_title' );
$tcu_image     = 'showcase_page_repeater_image';
$tcu_title     = 'showcase_page_repeater_title';
$tcu_content   = 'showcase_page_repeater_content';
$tcu_text      = 'showcase_page_repeater_link_text';
$tcu_link      = 'showcase_page_repeater_url';
$tcu_arialabel = 'showcase_page_repeater_aria_label';

/*
* Returns the HTML component of the showcase
* located in functions.php
*/
tcu_showcase_template( $tcu_rows, $tcu_heading, $tcu_image, $tcu_title, $tcu_content, $tcu_link, $tcu_text, $tcu_arialabel );

