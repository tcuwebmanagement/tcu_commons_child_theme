<?php
/**
 * Template Name: Flexible Content
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

get_header();

if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<!--
We give this elemen an ID of main because
we have a skip to main link.
-->
<main id="main">

	<?php
	// check if the flexible content field has rows of data.
	if ( have_rows( 'the_commons_flexible_layout_sections' ) ) :

		// loop through the rows of data.
		while ( have_rows( 'the_commons_flexible_layout_sections' ) ) :
			the_row();

			if ( get_row_layout() === 'the_commons_large_hero_image_section' ) :

				get_template_part( 'partials/content', 'heroimage' );

			elseif ( get_row_layout() === 'the_commons_large_video_section' ) :

				get_template_part( 'partials/content', 'video' );

			elseif ( get_row_layout() === 'the_commons_highlight_sections_two_columns' ) :

				get_template_part( 'partials/content', 'highlightbox' );

			elseif ( get_row_layout() === 'the_commons_highlight_sections_three_columns' ) :

				get_template_part( 'partials/content', 'threecards' );

			elseif ( get_row_layout() === 'the_commons_expandable_banner' ) :

				get_template_part( 'partials/content', 'expandablebanner' );

			elseif ( get_row_layout() === 'the_commons_tabs_section' ) :

				get_template_part( 'partials/content', 'tabsection' );

			elseif ( get_row_layout() === 'the_commons_feature_section' ) :

				get_template_part( 'partials/content', 'feature' );

			elseif ( get_row_layout() === 'the_commons_visual_editor' ) :

				get_template_part( 'partials/content', 'visualeditor' );

			elseif ( get_row_layout() === 'the_commons_showcase_section' ) :

				get_template_part( 'partials/content', 'showcase' );

			elseif ( get_row_layout() === 'the_commons_active_calendar_feed' ) :

				get_template_part( 'partials/content', 'calendarfeed' );

			elseif ( get_row_layout() === 'the_commons_tcu_news_section' ) :

				get_template_part( 'partials/content', 'tcunews' );

			elseif ( get_row_layout() === 'rss_tcu_news_feed' ) :

				get_template_part( 'partials/content', 'rss-news' );

			elseif ( get_row_layout() === 'the_commons_large_slider_section' ) :

				get_template_part( 'partials/content', 'slider' );

			elseif ( get_row_layout() === 'the_commons_content_blinds_section' ) :

				get_template_part( 'partials/content', 'content-blinds' );

			elseif ( get_row_layout() === 'the_commons_page_hero_image' ) :

				get_template_part( 'partials/content', 'pagehero' );

			endif;

		endwhile;
		?>

</main><!-- end of #main -->

<?php
else :
	get_template_part( 'partials/content', 'none' );
endif;

get_footer();

?>
