## TCU Commons Child Theme

Author: Mayra Perales <mailto:m.j.perales@tcu.edu>

---

**v2.3.3**

-   Fix tabcordion IDs to be unique.

**v2.3.2**

-   Added correct form attributes to search form

**v2.3.1**

-   Style for High Contrast Mode in IE

**v2.3.0**

-   Add PHPCS, ESLint, SassLint
-   Formatted theme to fit WordPress coding standards
-   Removed WP Advance Search form
-   Update h2 to h1 to follow the parent theme
-   Update slick.js version to 1.8
-   Add focusable="false" to SVG icons
-   Slowed down the slider
-   Update featured section color and font size to pass WCAG 2.0
-   Created theme settings page to include TCU's Google Search Appliance
-   Escaped front end input
-   Removed unnecessary aria roles, section and article HTML tags

**v2.2.5**

-   Changed the heading tag in the slider to an H2
-   Removed visited state for buttons
-   Loaded JS files only on the page templates it needs them
-   Theme does not allow you to install it without ACF plugin
-   Added aria-label to News/RSS feed Read More buttons

**v2.2.4**

-   Added focus style to elements in homepage
-   Added alt attribute to images
-   Increased the link size in hero image section
-   Slider now auto plays
-   Removed background image from mobile view of video section
-   Added aria-label to sections in flexible content
-   Added aria-label to showcase-page template
-   Added aria-label to heroimage-page template

**v2.1.4**

-   Fixed the breadcrumbs on CPT taxonomy
-   Fixed the table that display the search results
-   Increased arrow/dots size in slider
-   Styled tag cloud
-   Made some accessibility enhancements

**v2.1.3**

-   Created a hero image template function that displays that section
-   Created a showcase template function that displays that section
-   Cleaned up documentation
-   Run tcushowcase.js only on large screen size

**v2.0.3**

-   Changed styling of buttons within the 3 column highlight section
-   Added ability to change the text for links in 3/2 columns
-   Increased heading size in hero image section
-   Content area is now a visual editor
-   Added ability to change link text in content area
-   Fixed empty p tags within two/three column shortcode

**v2.0.2**

-   Showcase works when window is resized
-   Removed black overlay in hero image and 3 column cards
-   Added right padding to 3 column cards
-   Fixed feature section not centering
-   Added ability to add shortcodes into Calendar section
-   Changed Calendar section name to Content Area (Shortcodes)
-   Changed some ACF fields to not required

**v2.0.1**

-   Fixed showcase for flexible content
-   Added a showcase page template
-   Styled hero image to match hero image template
-   Centered blocks in expandable section
-   Removed two-column with buttons section
-   Styled headings to include svg graphic
-   Deleted Areas of Study and Overview page templates
-   Bolded all flexible content titles
-   Added RSS feed section to flexible content
-   Pointed to min CSS version
-   Fixed tcu_acf_load_value function

**v2.0.0**

-   Added heading field to content blind feature enhancement
-   Added button to content boxes
-   Added tcu- prefix to content boxes for consitency

-   Added adjustments to content blind feature enhancement
-   Added Content blind JS to the main JS file instead of seperate file
-   Removed height from Content blind container(was for testing purposes)
-   Added background texture to content blind container

-   Added content blind feature enhancement
-   Needs adjustments to code both in backend and concat of JS

-   Fixed background image URL broken url path.
-   Added custom field for page hero heading/title
-   Added video SVG to the template file via CSS background image

**v1.0.5**

-   Added custom fields for page hero images
-   Removed button class from the page hero
-   Adjusted heading and content font size to match design per Jenny

**v1.0.4**

-   Removed typographic background image from visual editor section
-   Added new classes for SVG graphic and background

**v1.0.3**

-   Added grunt-sass tasks to watch css files
-   Added icon for video background
-   Changed button color for calendar section

**v1.0.2**

-   Changed font for highlight boxes to capital case
-   Added subtle background image to the Calendar section of homepage

**v1.0.1**

-   Changed text/arrow color in expandable banner section
-   Fixed empty button link in expandable banner section
-   Adjusted headings in flexible content
-   Added fallback background color to highlight cards

**v1.0.0**

-   Initial theme
