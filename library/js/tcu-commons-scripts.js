// as the page loads, call these scripts
jQuery( document ).ready( function( $ ) {
    var arrowButton = $( '.tcu-arrow' );
    var expandableBanner = $( '.tcu-expandablebanner-background' );

    // Hide items within the tcu-expandable-background
    expandableBanner.css( 'display', 'none' );

    // click event
    arrowButton.click( function() {

        // Toggle aria-expanded
        if ( 'false' === $( this ).attr( 'aria-expanded' ) ) {
            $( this )
                .attr( 'aria-expanded', 'true' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#up-arrow"></use></svg><span class="tcu-visuallyhidden">Contract Menu</span>'
                );
        } else {
            $( this )
                .attr( 'aria-expanded', 'false' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#down-arrow"></use></svg><span class="tcu-visuallyhidden">Expand Menu</span>'
                );
        }

        // Toggle slide
        expandableBanner.slideToggle();
    } );

    // Close menu when ESC key is pressed
    window.addEventListener( 'keydown', function( e ) {

        // If ESC key is pressed
        if ( 'Escape' === e.code || 27 === e.which ) {
            arrowButton
                .attr( 'aria-expanded', 'false' )
                .attr( 'tabindex', '0' )
                .html(
                    '<svg height="40" width="40"><use focusable="false" xlink:href="#down-arrow"></use></svg><span class="tcu-visuallyhidden">Expand Menu</span>'
                );

            // Toggle slide
            expandableBanner.css( 'display', 'none' );
        }
    } );

    /*  IF YOU WANT TO USE DEVICE.JS TO DETECT THE VIEWPORT AND MANIPULATE THE OUTPUT  */

    //Device.js will check if it is Tablet or Mobile - http://matthewhudson.me/projects/device.js/
    /* global device */
    if ( ! device.tablet() && ! device.mobile() ) {
        $( '#ytplayer' ).css( 'display', 'block' );
    } else {

        //jQuery will add the default background to the preferred class
        $( '.tcu-video' ).css( 'display', 'none' );
        $( '#ytplayer' ).css( 'display', 'none' );
    }

    $( '.tcu-commons-slider' ).slick( {
        dots: true,
        arrows: true,
        infinite: true,
        mobileFirst: true,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 6000,
        prevArrow:
            '<button type="button" data-role="none" class="slick-prev tcu-pulse" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow:
            '<button type="button" data-role="none" class="slick-next tcu-pulse" aria-label="Next" tabindex="0" role="button">Next</button>'
    } );

    /* Begin Showcase  */
    $( '.tcu-showcase-wrapper' ).tcushowcase();
} ); /* end of as page load scripts */

( function() {
    var tcuMasonryHome = document.getElementById( 'tcu-masonry-home' );

    if ( null === tcuMasonryHome ) {
        return;
    }

    /* global AnimOnScroll */
    var homeMasonryAnim = new AnimOnScroll( tcuMasonryHome, {
        minDuration: 0.4,
        maxDuration: 0.7,
        viewportFactor: 0.2
    } );

    homeMasonryAnim._init();
} () );
