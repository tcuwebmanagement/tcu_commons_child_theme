/**
 * TCU Showcase
 * Author: Mayra Perales <m.j.perales@tcu.edu>
 */
( function( $ ) {
    'use strict';

    $.fn.tcushowcase = function( options ) {
        var defaults = {
            tcuShowcaseElement: jQuery( this ),
            tcuShowcaseContent: $( '.tcu-showcase__content' ),
            tcuShowcaseContentLast: $( '.tcu-showcase__content:last' ),
            tcuShowcaseLast: $( '.tcu-showcase:last' ),
            tcuShowcaseImage: $( '.tcu-showcase .tcu-showcase__image' ),
            tcuMinScreenWidth: '1200' // Width for showcase to activate
        };

        options = $.extend( defaults, options );

        return this.each( function() {
            var tcuShowcaseElement = options.tcuShowcaseElement;
            var tcuShowcaseContent = options.tcuShowcaseContent;
            var tcuShowcaseContentLast = options.tcuShowcaseContentLast;
            var tcuShowcaseLast = options.tcuShowcaseLast;
            var tcuShowcaseImage = options.tcuShowcaseImage;
            var tcuMinScreenWidth = options.tcuMinScreenWidth;

            // get browser width
            var tcuCurrentWindowWidth = window.innerWidth || document.documentElement.clientWidth;

            // detect known mobile/tablet
            var isMobile = false;
            if (
                navigator.userAgent.match( /iPhone/i ) ||
                navigator.userAgent.match( /iPod/i ) ||
                navigator.userAgent.match( /iPad/i ) ||
                navigator.userAgent.match( /Android/i ) ||
                navigator.userAgent.match( /Blackberry/i ) ||
                navigator.userAgent.match( /Windows Phone/i )
            ) {
                isMobile = true;
            }

            var tcuShowcaseActive = false;

            /**
             * Main function
             */
            function tcuShowcase() {
                if ( ! tcuShowcaseActive ) {

                    /**
                     * Add .tcu-showcase--inactive to element
                     */
                    jQuery( tcuShowcaseElement )
                        .children()
                        .addClass( 'tcu-showcase--inactive' );

                    /**
                     * Make our input keyboard accessible
                     */
                    jQuery( tcuShowcaseImage ).attr( 'tabindex', '0' );

                    /**
                     * Add .tcu-showcase--hide to .tcu-showcase__content
                     */
                    jQuery( tcuShowcaseContent ).addClass( 'tcu-showcase--hide' );

                    /**
                     *  Remove tcu-showcase--inactive and
                     * add the tcu-showcase--active class
                     * to the last element
                     */
                    jQuery( tcuShowcaseLast )
                        .removeClass( 'tcu-showcase--inactive' )
                        .addClass( 'tcu-showcase--active' );

                    /**
                     * Remove .tcu-showcase--hide
                     * and replace with .tcu-showcase--show
                     * to the last .tcu-showcase__content element
                     */
                    jQuery( tcuShowcaseContentLast )
                        .removeClass( 'tcu-showcase--hide' )
                        .addClass( 'tcu-showcase--show' );

                    /**
                     * On click
                     */
                    jQuery( tcuShowcaseImage ).on( 'click', function() {
                        if (
                            $( this )
                                .parent()
                                .is( '.tcu-showcase--inactive' )
                        ) {

                            // Find active element and toggle active/inactive
                            jQuery( '.tcu-showcase--active' )
                                .toggleClass( 'tcu-showcase--active' )
                                .toggleClass( 'tcu-showcase--inactive' );

                            // Find active element's content and toggle show/hide
                            jQuery( '.tcu-showcase--show' )
                                .toggleClass( 'tcu-showcase--show' )
                                .toggleClass( 'tcu-showcase--hide' );

                            // Find selected element and toggle inactive/active class
                            jQuery( this )
                                .parent()
                                .toggleClass( 'tcu-showcase--inactive' )
                                .toggleClass( 'tcu-showcase--active' );

                            // Find selected element's content div and toggle hide/show class
                            jQuery( this )
                                .parent()
                                .find( '.tcu-showcase__content' )
                                .toggleClass( 'tcu-showcase--hide' )
                                .toggleClass( 'tcu-showcase--show' );
                        } else {

                            // Deactivate current hovered item
                            jQuery( this )
                                .parent()
                                .removeClass( 'tcu-showcase--active' )
                                .addClass( 'tcu-showcase--inactive' );

                            // hide tcu-showcase__content
                            jQuery( this )
                                .parent()
                                .find( '.tcu-showcase__content' )
                                .removeClass( 'tcu-showcase--show' )
                                .addClass( 'tcu-showcase--hide' );

                            jQuery( tcuShowcaseLast )
                                .removeClass( 'tcu-showcase--inactive' )
                                .addClass( 'tcu-showcase--active' );

                            jQuery( tcuShowcaseContentLast )
                                .removeClass( 'tcu-showcase--hide' )
                                .addClass( 'tcu-showcase--show' );
                        }
                    } );

                    tcuShowcaseActive = true;
                }
            }

            /**
             *  re-instate original nav (and call this on window.width functions)
             */
            function tcuShowcaseOriginal() {
                jQuery( tcuShowcaseElement )
                    .children()
                    .removeClass( 'tcu-showcase--inactive' );

                jQuery( tcuShowcaseElement )
                    .children()
                    .removeClass( 'tcu-showcase--active' );

                jQuery( tcuShowcaseImage ).attr( 'tabindex', '-1' );

                jQuery( tcuShowcaseContent ).removeClass( 'tcu-showcase--hide' );
                jQuery( tcuShowcaseContent ).removeClass( 'tcu-showcase--show' );
                tcuShowcaseActive = false;
            }

            // reset menu on resize above tcuMinScreenWidth
            if ( isMobile ) {
                jQuery( window ).resize( function() {
                    var currentWidth = window.innerWidth || document.documentElement.clientWidth;

                    if ( currentWidth < tcuMinScreenWidth ) {
                        tcuShowcaseOriginal();
                    }
                } );
            }

            jQuery( window ).resize( function() {

                // get browser width
                var currentWidth = window.innerWidth || document.documentElement.clientWidth;

                if ( currentWidth <= tcuMinScreenWidth ) {
                    tcuShowcaseOriginal();
                } else {
                    tcuShowcase();
                }
            } );

            // run our function on screen size larger than tcuMinScreenWidth
            if ( tcuCurrentWindowWidth > tcuMinScreenWidth ) {
                tcuShowcase();
            }
        } );
    };
} ( jQuery ) );
