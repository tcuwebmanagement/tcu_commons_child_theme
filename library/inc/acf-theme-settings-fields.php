<?php
/**
 * The ACF fields for the settings page
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 2.3.0
 */

add_action( 'acf/init', 'tcu_commons_child_theme_settings' );

/**
 * ACF callback function
 */
function tcu_commons_child_theme_settings() {

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5ad4f376c6993',
			'title'                 => 'The Commons Theme - Settings',
			'fields'                => array(
				array(
					'key'               => 'field_5ad4f8e8e8a2c',
					'label'             => 'Replace search form?',
					'name'              => 'the_commons_search_form',
					'type'              => 'true_false',
					'instructions'      => 'Do you want to replace the WordPress search form with Google Search Appliance?',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'message'           => '',
					'default_value'     => 0,
					'ui'                => 1,
					'ui_on_text'        => '',
					'ui_off_text'       => '',
				),
				array(
					'key'               => 'field_5ad4f9ad6fab1',
					'label'             => 'Collection Name',
					'name'              => 'the_commons_collection_name',
					'type'              => 'text',
					'instructions'      => 'Find the collection name by login in to the Google Search Appliance dashboard > Index > Collections.',
					'required'          => 1,
					'conditional_logic' => array(
						array(
							array(
								'field'    => 'field_5ad4f8e8e8a2c',
								'operator' => '==',
								'value'    => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => 'default_collection',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
				array(
					'key'               => 'field_5ad4faaf6fab2',
					'label'             => 'Front End Name',
					'name'              => 'tcu_commons_frontend_name',
					'type'              => 'text',
					'instructions'      => 'Find the front end name by login in to the Google Search Appliance dashboard > Search > Front Ends.',
					'required'          => 1,
					'conditional_logic' => array(
						array(
							array(
								'field'    => 'field_5ad4f8e8e8a2c',
								'operator' => '==',
								'value'    => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => 'universal_webstandards',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'theme-general-settings',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);

}
