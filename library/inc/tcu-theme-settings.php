<?php
/**
 * Theme Settings
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 2.3.0
 */

add_action( 'acf/init', 'tcu_commons_settings' );

/**
 * ACF callback function
 */
function tcu_commons_settings() {

	if ( function_exists( 'acf_add_options_page' ) ) {

		$option_page = acf_add_options_page(
			array(
				'page_title'  => __( 'Theme Settings', 'tcu_commons_child_theme' ),
				'menu_slug'   => 'theme-general-settings',
				'capability'  => 'manage_options',
				'parent_slug' => 'options-general.php',
			)
		);

	}
}


