<?php
/**
 * Hero page ACF fields
 *
 * @package tcu_commons_child_theme
 * @since TCU Commons Child Theme 1.0.0
 */

add_action( 'acf/init', 'tcu_commons_child_theme_page_hero_fields' );

/**
 * ACF fields
 */
function tcu_commons_child_theme_page_hero_fields() {
	acf_add_local_field_group(
		array(
			'key'                   => 'group_597901d91bded',
			'title'                 => 'Hero Image Section for pages',
			'fields'                => array(
				array(
					'key'               => 'field_597901f6d5151',
					'label'             => 'Hero Image',
					'name'              => 'the_commons_page_hero_image',
					'type'              => 'image',
					'instructions'      => 'Upload an image to use for the page hero.',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'return_format'     => 'array',
					'preview_size'      => 'tcu-700-550',
					'library'           => 'all',
					'min_width'         => '',
					'min_height'        => '',
					'min_size'          => '',
					'max_width'         => '',
					'max_height'        => '',
					'max_size'          => '',
					'mime_types'        => '',
				),
				array(
					'key'               => 'field_597903cad5152',
					'label'             => 'Title',
					'name'              => 'the_commons_page_hero_heading',
					'type'              => 'text',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
				array(
					'key'               => 'field_597903ead5153',
					'label'             => 'Content',
					'name'              => 'the_commons_page_hero_image_content',
					'type'              => 'textarea',
					'instructions'      => 'Enter the content for the page hero.',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'maxlength'         => 200,
					'rows'              => '',
					'new_lines'         => 'wpautop',
				),
				array(
					'key'               => 'field_59790438d5154',
					'label'             => 'Link',
					'name'              => 'the_commons_page_hero_link',
					'type'              => 'url',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
				),
				array(
					'key'               => 'field_59790457d5155',
					'label'             => 'Link Text',
					'name'              => 'the_commons_page_hero_link_text',
					'type'              => 'text',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => 'Learn More',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
				array(
					'key'               => 'field_5979047ad5156',
					'label'             => 'Aria-label',
					'name'              => 'the_commons_page_hero_aria_label',
					'type'              => 'text',
					'instructions'      => 'The objective is to describe the purpose of the link. Example: Read more about Seminole\'s new baby mayor. <a targe="_blank" href="https://www.w3.org/TR/WCAG20-TECHS/ARIA8">Read more about the correct way to implement the aria-label</a>',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'page-with-hero.php',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'acf_after_title',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);

}
